'''
Создать класс TrafficLight (светофор) и определить у него один атрибут color (цвет) и метод running (запуск).
Атрибут реализовать как приватный.
В рамках метода реализовать переключение светофора в режимы: красный, желтый, зеленый.
Продолжительность первого состояния
 (красный) составляет 7 секунд,
 второго (желтый) — 2 секунды,
 третьего (зеленый) — на ваше усмотрение. Переключение между режимами должно осуществляться только в указанном порядке (красный, желтый, зеленый).
Проверить работу примера, создав экземпляр и вызвав описанный метод.
'''
import sys
import time


class TrafficLight:
    __color = ['Red', 'Yellow', 'Green']

    def __exsleep(self, timeout: int, color_idx: int):
        for remaining in range(timeout, 0, -1):
            sys.stdout.write("\r")
            sys.stdout.write(f"Next color is {self.__color[color_idx]}, after {remaining} sec.")
            sys.stdout.flush()
            time.sleep(1)

    def running(self):
        i = 0
        while i <= 2:
            print(f"\nCurrent light is {self.__color[i]}")
            if i == 0:
                self.__exsleep(7, i + 1)
            elif i == 1:
                self.__exsleep(5, i + 1)
            elif i == 2:
                self.__exsleep(6, i - i)
            i += 1
        print("\nFinish!")


TrafficLight = TrafficLight()
TrafficLight.running()
