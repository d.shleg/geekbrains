"""
Программа запрашивает у пользователя строку чисел, разделенных пробелом.
При нажатии Enter должна выводиться сумма чисел.
Пользователь может продолжить ввод чисел, разделенных пробелом и снова нажать Enter.
Сумма вновь введенных чисел будет добавляться к уже подсчитанной сумме.
Но если вместо числа вводится специальный символ, выполнение программы завершается.
Если специальный символ введен после нескольких чисел, то вначале нужно добавить сумму этих чисел к полученной ранее сумме и после этого завершить программу.
"""

def sum_digits():
    result_total = 0
    stop_state = False

    while not stop_state:
        result = 0
        userinput = input("Please enter some digits separated by space. For exit enter 'q': ").split()

        #Apply float function for digits or str for others
        userinput_typed = list(map(lambda x: float(x) if x.isdigit() else str(x), userinput))
        stop_state = (False, True)['q' in list(filter(lambda x: isinstance(x, str), userinput_typed))]
        result += sum(filter(lambda x: isinstance(x, (int, float)), userinput_typed))

        print(f'Sum of last entered digits: {result}')
        result_total += result

    print(f'Total sum: {result_total}')

sum_digits()