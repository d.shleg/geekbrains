"""
Программа принимает действительное положительное число x и целое отрицательное число y.
Необходимо выполнить возведение числа x в степень y.
Задание необходимо реализовать в виде функции my_func(x, y).
При решении задания необходимо обойтись без встроенной функции возведения числа в степень.
"""


def my_func(x, y):
    if x < 0:
        return "The base must be a positive number( >0 )!"
    elif y > 0:
        return "The degree must be negative!( <0 )"
    return x ** y


def my_func_power(x, y):
    if x < 0:
        return "The base must be a positive digit( >0 )!"
    elif y > 0:
        return "The degree must be negative digit!( <0 )"

    result = 1
    for d in range(abs(y)):
        result *= x

    return 1 / result


print(
    my_func(
        float(input("Enter positive digit:")),
        int(input("Enter unsigned negative digit: ")))
)

print(
    my_func_power(
        float(input("Enter positive digit:")),
        int(input("Enter unsigned negative digit: ")))
)