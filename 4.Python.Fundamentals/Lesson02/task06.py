"""
*Реализовать структуру данных «Товары». Она должна представлять собой список кортежей.
Каждый кортеж хранит информацию об отдельном товаре.
В кортеже должно быть два элемента — номер товара и словарь с параметрами (характеристиками товара: название, цена, количество, единица измерения).
Структуру нужно сформировать программно, т.е. запрашивать все данные у пользователя.
Пример готовой структуры:

[
    (1, {“название”: “компьютер”, “цена”: 20000, “количество”: 5, “eд”: “шт.”}),
    (2, {“название”: “принтер”, “цена”: 6000, “количество”: 2, “eд”: “шт.”}),
    (3, {“название”: “сканер”, “цена”: 2000, “количество”: 7, “eд”: “шт.”})
]
Необходимо собрать аналитику о товарах. Реализовать словарь, в котором каждый ключ — характеристика товара, например название, а значение — список значений-характеристик, например список названий товаров.
Пример:

{
“название”: [“компьютер”, “принтер”, “сканер”],
“цена”: [20000, 6000, 2000],
“количество”: [5, 2, 7],
“ед”: [“шт.”]
}
"""

products = []
product_params = {"name": '', "price": 0, "quantity": 0, "units": ''}
product_counter = 1
analytics = {"name": [], "price": [], "quantity": [], "units": []}

while input("\nAdd new product? Enter yes/no: ") == 'yes':

    print(product_counter)
    product_param = {}
    for param in product_params:
        param_value = input(f"Enter product param {param}: ")

        if param_value.isdigit() and type(product_params[param]) is int:
            product_param[param] = int(param_value)
            analytics[param].append(int(param_value))
        else:
            product_param[param] = param_value
            analytics[param].append(param_value)

        analytics[param] = list(set(analytics[param]))
        analytics[param].sort()

    products.append(tuple([product_counter, product_param]))
    product_counter += 1

print(f"Products: {products}\nAnalytics: {analytics}")
