"""
Пользователь вводит месяц в виде целого числа от 1 до 12.
Сообщить к какому времени года относится месяц (зима, весна, лето, осень).
Напишите решения через list и через dict.
"""
seasons_list = ['Winter', 'Spring', 'Summer', 'Autumn']
seasons_dict = {'Winter': (1, 2, 12), 'Spring': (3, 4, 5), 'Summer': (6, 7, 8), 'Autumn': (9, 10, 11)}
list_result = None
month = int(input("Please enter month number without leading zeros: "))

if 12 >= month >= 1:

    # Solution with Dictionary
    for key in seasons_dict.keys():
        if month in seasons_dict[key]:
            print(f"Season name from dict is: {key}")
else:
  print("Incorrect month number!")




# Solution with List
if month == 1 or month == 2 or month == 12:
    list_result = seasons_list[0]
elif month == 3 or month == 4 or month == 5:
    list_result = seasons_list[1]
elif month == 6 or month == 7 or month == 8:
    list_result = seasons_list[2]
elif month == 9 or month == 10 or month == 11:
    list_result = seasons_list[3]
else:
    print("Incorrect month number!")


print(f"Season name from list is: {list_result}")