"""
Для списка реализовать обмен значений соседних элементов,
т.е. Значениями обмениваются элементы с индексами 0 и 1, 2 и 3 и т.д.
При нечетном количестве элементов последний сохранить на своем месте.
Для заполнения списка элементов необходимо использовать функцию input().
"""

new_list = []
new_value = None

while new_value != 'stop':
    new_value = input("For fill list, enter single or list values separated by commas. for finish enter stop: ")

    if "," in new_value:
        new_list.extend(new_value.split(','))
    elif new_value == "stop":
        continue
    else:
        new_list.append(new_value)

print(f"Entered list values: {new_list}")

if len(new_list) % 2 == 0:
    i = 0
    while i < len(new_list):
        new_list[i], new_list[i + 1] = new_list[i + 1], new_list[i]
        i += 2

    print(f"Entered even list values: {new_list}")

else:
    i = 0
    while i < len(new_list) - 1:
        new_list[i], new_list[i + 1] = new_list[i + 1], new_list[i]
        i += 2

    print(f"Entered odd list values: {new_list}")
