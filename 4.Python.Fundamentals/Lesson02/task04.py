"""
 Пользователь вводит строку из нескольких слов, разделённых пробелами.
 Вывести каждое слово с новой строки. Строки необходимо пронумеровать.
 Если в слово длинное, выводить только первые 10 букв в слове.
"""
userinput = input("Please enter some words separated by space: ")

new_list = userinput.split(' ')
for key, value in enumerate(new_list, 1):
    print(key, value[0:10])
