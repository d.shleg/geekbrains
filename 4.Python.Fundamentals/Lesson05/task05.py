'''
Создать (программно) текстовый файл, записать в него программно набор чисел, разделенных пробелами.
Программа должна подсчитывать сумму чисел в файле и выводить ее на экран.
'''
from typing import List

some_string = input("Please enter some digits separated by space: ")
file_sum = 0
with open('file05.txt', 'w+') as fh:
    fh.writelines(some_string)
    fh.seek(0)
    file_string = fh.read().splitlines()
    file_sum = sum(list(map(int, file_string[0].split())))

print(f"Sum of digits in file is: {file_sum}")
      