'''
Создать текстовый файл (не программно), построчно записать фамилии сотрудников и величину их окладов (не менее 10 строк).
 пределить, кто из сотрудников имеет оклад менее 20 тыс.,
 вывести фамилии этих сотрудников.
 Выполнить подсчет средней величины дохода сотрудников.
Пример файла:

Иванов 23543.12
Петров 13749.32
'''

with open("file03.txt", encoding='utf-8') as fh:
    poor_salary = []
    sal = []
    employee_list = list(filter(None, fh.read().splitlines()))

    for em in employee_list:
        name, salary = em.split()
        if float(salary) < 20000:
            poor_salary.append(name)
        sal.append(float(salary))

print(f"Employes with salary < 20000 is: {poor_salary}")
print(f"Average salary {round(sum(sal) / len(sal))}")
