'''
Реализовать проект «Операции с комплексными числами».
 Создайте класс «Комплексное число», реализуйте перегрузку методов сложения и умножения комплексных чисел.
  Проверьте работу проекта, создав экземпляры класса (комплексные числа) и выполнив сложение и умножение созданных экземпляров.
 Проверьте корректность полученного результата.
'''
class Complex:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def __add__(self, other):
        return f'Sum eq: {self.a + other.a} + {self.b + other.b} * i'

    def __mul__(self, other):
        return f'Multiply eq: {self.a * other.a - (self.b * other.b)} + {self.b * other.a} * i'


c_1 = Complex(7, -7)
c_2 = Complex(6, 12)
print(c_1 + c_2)
print(c_1 * c_2)