'''
Создайте собственный класс-исключение, обрабатывающий ситуацию деления на нуль.
Проверьте его работу на данных, вводимых пользователем.
При вводе пользователем нуля в качестве делителя программа должна корректно обработать эту ситуацию и не завершиться с ошибкой.
'''


class MyDivideByZero(Exception):
    text: str = "Division by zero not allowed"

    def __str__(self):
        return self.text


dividend, divider = map(int, input("Enter dividend and divider separated by space:").split())
try:
    if divider == 0:
        raise MyDivideByZero
    print(dividend / divider)
except MyDivideByZero as e:
    print(e)
