'''
Реализовать класс «Дата», функция-конструктор которого должна принимать дату в виде строки формата «день-месяц-год».
В рамках класса реализовать два метода. Первый, с декоратором @classmethod, должен извлекать число, месяц, год и преобразовывать их тип к типу «Число».
Второй, с декоратором @staticmethod, должен проводить валидацию числа, месяца и года (например, месяц — от 1 до 12).
 Проверить работу полученной структуры на реальных данных.
'''

import datetime


class Date:
    date_string: str

    def __init__(self, date_string: str):
        self.date_string = str(date_string)

    @classmethod
    def parse(cls, date_string):
        date_chunks = date_string.replace(" ", "").split('-')
        # return list(map(int, date_chunks))
        return "Day:{0}, Month:{1}, Year:{2}".format(*list(map(int, date_chunks)))

    @staticmethod
    def validate(date_string):
        try:
            datetime.datetime.strptime(date_string, '%d-%m-%Y')
            return date_string
        except ValueError:
            raise ValueError("Incorrect date format.")

    def __str__(self):
        return f"Current date {Date.parse(self.date_string)}"


result = print(Date('05 - 4-2021'))
print(Date.validate("31-12-2020"))
